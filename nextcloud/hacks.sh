#!/bin/sh
set -ex

occ () {
  su -p www-data -s /bin/sh -c "/var/www/html/occ $1"
}



if [ -z "$NEXTCLOUD_ADMIN_USER" ]; then
  echo "[WARNING]: Using default admin user name"
  NEXTCLOUD_ADMIN_USER="admin2"
fi

if [ -z "$NEXTCLOUD_ADMIN_PASSWORD" ]; then
  echo "[WARNING]: Using default admin password"
  NEXTCLOUD_ADMIN_PASSWORD="admin2"
fi

#occ "maintenance:install --admin-user $NEXTCLOUD_ADMIN_USER --admin-pass $NEXTCLOUD_ADMIN_PASSWORD"

echo "[NOTICE]: Trusted domains are $ANEXTCLOUD_TRUSTED_DOMAINS"
for domain in $ANEXTCLOUD_TRUSTED_DOMAINS; do
  echo "[NOTICE]: Setting trusted domain to $domain"
  occ "config:system:set trusted_domains 3 --value=$domain"
done

for app in $NEXTCLOUD_APPS; do
  occ "app:install $app" || true
  occ "app:enable $app"
done

for app in $NEXTCLOUD_DISABLED_APPS; do
        occ "app:disable $app"
done

cat /var/www/html/config/config.php
